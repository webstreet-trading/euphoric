<?php

class WS_Builder_Module_Blog extends ET_Builder_Module_Blog {
    
    /**
     * The string used to uniquely identify
     * 
     * @var string 
     */
	public $textdomain = null;
    
    /**
     * Init module
     */
    function init() {
        parent::init();
        
        $this->textdomain = WS::$textdomain;
        
        $this->name = esc_html__('Blog Post Type', $this->textdomain);
        $this->slug = 'ws_et_pb_blog';
        
        $this->whitelisted_fields[] = 'post_type';
        $this->whitelisted_fields[] = 'taxonomy';
        $this->whitelisted_fields[] = 'include_taxonomies';
        $this->whitelisted_fields[] = 'output_format';
        
        $this->fields_defaults['post_type'] = array('post', 'add_default_setting');
        $this->fields_defaults['output_format'] = array('%1$s %2$s %3$s %4$s', 'add_default_setting');
    }
    
    /**
     * Get module fields
     */
    function get_fields() {
        $fields = parent::get_fields();
        
        $post_type_objects = get_post_types(array('public' => true), 'objects');
        $post_types = array();
        foreach ($post_type_objects as $post_type => $post_type_object) {
            $post_types[$post_type] = $post_type_object->labels->name;
        }
        
        // add post type and taxononmy fields
        $fields = array_merge(array(
            'post_type' => array(
                'label' => esc_html__('Post Type', $this->textdomain),
                'type' => 'select',
                'option_category' => 'layout',
                'options' => $post_types,
                'toggle_slug' => 'main_content',
                'computed_affects' => array(
                    '__posts',
                ),
            ),
            'taxonomy' => array(
                'label' => esc_html__('Taxonomy slug', $this->textdomain),
                'description' => esc_html__('Taxononmy to use for below include.', $this->textdomain),
                'type' => 'text',
                'option_category' => 'configuration',
                'computed_affects' => array(
                    '__posts',
                ),
                'toggle_slug' => 'main_content',
            ),
            'include_taxonomies' => array(
                'label' => esc_html__('Include Taxonomy slug(s)', $this->textdomain),
                'description' => esc_html__('Comma delimited list of taxonomy slug(s) to select. Leave empty to include all.', $this->textdomain),
                'type' => 'text',
                'option_category' => 'configuration',
                'computed_affects' => array(
                    '__posts',
                ),
                'toggle_slug' => 'main_content',
            ),
            'output_format' => array(
                'label' => esc_html__('Output format for posts', $this->textdomain),
                'description' => esc_html__('Graphic -> %1$s | Title -> %2$s | Post Meta -> %3$s | Content -> %4$s', $this->textdomain),
                'type' => 'text',
                'option_category' => 'configuration',
                'computed_affects' => array(
                    '__posts',
                ),
                'toggle_slug' => 'main_content',
            ),
        ), $fields);
        
        $fields['__posts']['computed_depends_on'][] = 'post_type';
        $fields['__posts']['computed_depends_on'][] = 'taxonomy';
        $fields['__posts']['computed_depends_on'][] = 'include_taxonomies';
        $fields['__posts']['computed_depends_on'][] = 'output_format';
        
        // hide categories
        $fields['include_categories']['toggle_slug'] = '';
        
        return $fields;
    }
    
    /**
     * Shortcode
     * 
     * @param array $atts
     * @param string $content
     * @param string $function_name
     */
    function shortcode_callback($atts, $content = null, $function_name) {
        
//        return parent::shortcode_callback($atts, $content, $function_name);
        
        /**
		 * Cached $wp_filter so it can be restored at the end of the callback.
		 * This is needed because this callback uses the_content filter / calls a function
		 * which uses the_content filter. WordPress doesn't support nested filter
		 */
		global $wp_filter;
		$wp_filter_cache = $wp_filter;

		$module_id           = $this->shortcode_atts['module_id'];
		$module_class        = $this->shortcode_atts['module_class'];
		$fullwidth           = $this->shortcode_atts['fullwidth'];
		$posts_number        = $this->shortcode_atts['posts_number'];
		$include_categories  = $this->shortcode_atts['include_categories'];
		$meta_date           = $this->shortcode_atts['meta_date'];
		$show_thumbnail      = $this->shortcode_atts['show_thumbnail'];
		$show_content        = $this->shortcode_atts['show_content'];
		$show_author         = $this->shortcode_atts['show_author'];
		$show_date           = $this->shortcode_atts['show_date'];
		$show_categories     = $this->shortcode_atts['show_categories'];
		$show_comments       = $this->shortcode_atts['show_comments'];
		$show_pagination     = $this->shortcode_atts['show_pagination'];
		$background_layout   = $this->shortcode_atts['background_layout'];
		$show_more           = $this->shortcode_atts['show_more'];
		$offset_number       = $this->shortcode_atts['offset_number'];
		$masonry_tile_background_color = $this->shortcode_atts['masonry_tile_background_color'];
		$use_dropshadow      = $this->shortcode_atts['use_dropshadow'];
		$overlay_icon_color  = $this->shortcode_atts['overlay_icon_color'];
		$hover_overlay_color = $this->shortcode_atts['hover_overlay_color'];
		$hover_icon          = $this->shortcode_atts['hover_icon'];
		$use_overlay         = $this->shortcode_atts['use_overlay'];
        
        $post_type = $this->shortcode_atts['post_type'];
        $taxonomy = $this->shortcode_atts['taxonomy'];
        $include_taxonomies = $this->shortcode_atts['include_taxonomies'];
        $output_format = $this->shortcode_atts['output_format'];

		global $paged;

		$module_class              = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
		$video_background          = $this->video_background();
		$parallax_image_background = $this->get_parallax_image_background();

		$container_is_closed = false;

		// some themes do not include these styles/scripts so we need to enqueue them in this module to support audio post format
		wp_enqueue_style( 'wp-mediaelement' );
		wp_enqueue_script( 'wp-mediaelement' );

		// include easyPieChart which is required for loading Blog module content via ajax correctly
		wp_enqueue_script( 'easypiechart' );

		// remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
		remove_all_filters( 'wp_audio_shortcode_library' );
		remove_all_filters( 'wp_audio_shortcode' );
		remove_all_filters( 'wp_audio_shortcode_class');

		if ( '' !== $masonry_tile_background_color ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%%.et_pb_blog_grid .et_pb_post',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_html( $masonry_tile_background_color )
				),
			) );
		}

		if ( '' !== $overlay_icon_color ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .et_overlay:before',
				'declaration' => sprintf(
					'color: %1$s !important;',
					esc_html( $overlay_icon_color )
				),
			) );
		}

		if ( '' !== $hover_overlay_color ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .et_overlay',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_html( $hover_overlay_color )
				),
			) );
		}

		if ( 'on' === $use_overlay ) {
			$data_icon = '' !== $hover_icon
				? sprintf(
					' data-icon="%1$s"',
					esc_attr( et_pb_process_font_icon( $hover_icon ) )
				)
				: '';

			$overlay_output = sprintf(
				'<span class="et_overlay%1$s"%2$s></span>',
				( '' !== $hover_icon ? ' et_pb_inline_icon' : '' ),
				$data_icon
			);
		}

		$overlay_class = 'on' === $use_overlay ? ' et_pb_has_overlay' : '';

		if ( 'on' !== $fullwidth ){
			if ( 'on' === $use_dropshadow ) {
				$module_class .= ' et_pb_blog_grid_dropshadow';
			}

			wp_enqueue_script( 'salvattore' );

			$background_layout = 'light';
		}

		$args = array( 'posts_per_page' => (int) $posts_number );

		$et_paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' );

		if ( is_front_page() ) {
			$paged = $et_paged;
		}

		if ( '' !== $include_categories )
			$args['cat'] = $include_categories;

		if ( ! is_search() ) {
			$args['paged'] = $et_paged;
		}

		if ( '' !== $offset_number && ! empty( $offset_number ) ) {
			/**
			 * Offset + pagination don't play well. Manual offset calculation required
			 * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
			 */
			if ( $paged > 1 ) {
				$args['offset'] = ( ( $et_paged - 1 ) * intval( $posts_number ) ) + intval( $offset_number );
			} else {
				$args['offset'] = intval( $offset_number );
			}
		}

		if ( is_single() && ! isset( $args['post__not_in'] ) ) {
			$args['post__not_in'] = array( get_the_ID() );
		}
        
        $args['post_type'] = $post_type;
        
        if ( '' !== $taxonomy && '' !== $include_taxonomies )
			$args['tax_query'] = array(
                array(
                    'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => $include_taxonomies,
                )
            );

		query_posts( $args );
        
        $output = array();

		if ( have_posts() ) {

			while ( have_posts() ) {
				the_post();
                
                $output_graphic = '';
                $output_title = '';
                $output_post_meta = '';
                $output_content = '';

				$post_format = et_pb_post_format();

				$thumb = '';

				$width = 'on' === $fullwidth ? 1080 : 400;
				$width = (int) apply_filters( 'et_pb_blog_image_width', $width );

				$height = 'on' === $fullwidth ? 675 : 250;
				$height = (int) apply_filters( 'et_pb_blog_image_height', $height );
				$classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
				$titletext = get_the_title();
				$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
				$thumb = $thumbnail["thumb"];

				$no_thumb_class = '' === $thumb || 'off' === $show_thumbnail ? ' et_pb_no_thumb' : '';

				if ( in_array( $post_format, array( 'video', 'gallery' ) ) ) {
					$no_thumb_class = '';
				}
                
                /**
                 * Graphic
                 */
                
                ob_start();
                
                et_divi_post_format_content();

				if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
					if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) :
						$video_overlay = has_post_thumbnail() ? sprintf(
							'<div class="et_pb_video_overlay" style="background-image: url(%1$s); background-size: cover;">
								<div class="et_pb_video_overlay_hover">
									<a href="#" class="et_pb_video_play"></a>
								</div>
							</div>',
							$thumb
						) : '';

						printf(
							'<div class="et_main_video_container">
								%1$s
								%2$s
							</div>',
							$video_overlay,
							$first_video
						);
					elseif ( 'gallery' === $post_format ) :
						et_pb_gallery_images( 'slider' );
					elseif ( '' !== $thumb && 'on' === $show_thumbnail ) :
						if ( 'on' !== $fullwidth ) echo '<div class="et_pb_image_container">'; ?>
							<a href="<?php esc_url( the_permalink() ); ?>" class="entry-featured-image-url">
								<?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
								<?php if ( 'on' === $use_overlay ) {
									echo $overlay_output;
								} ?>
							</a>
					<?php
						if ( 'on' !== $fullwidth ) echo '</div> <!-- .et_pb_image_container -->';
					endif;
				}
                
                $output_graphic = ob_get_clean();
                
                ?>
                <?php if ( 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
                    
                    /**
                     * Title
                     */

                    ob_start();
                    
                    if ( ! in_array( $post_format, array( 'link', 'audio' ) ) ) { ?>
                        <h2 class="entry-title"><a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a></h2>
                    <?php } ?>
                    <?php

                    $output_title = ob_get_clean();

                    /**
                     * Post meta
                     */
                    
                    if ( 'on' === $show_author || 'on' === $show_date || 'on' === $show_categories || 'on' === $show_comments ) {
                        $output_post_meta = sprintf( '<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s %7$s</p>',
                            (
                                'on' === $show_author
                                    ? et_get_safe_localization( sprintf( __( 'by %s', 'et_builder' ), '<span class="author vcard">' .  et_pb_get_the_author_posts_link() . '</span>' ) )
                                    : ''
                            ),
                            (
                                ( 'on' === $show_author && 'on' === $show_date )
                                    ? ' | '
                                    : ''
                            ),
                            (
                                'on' === $show_date
                                    ? et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' . esc_html( get_the_date( $meta_date ) ) . '</span>' ) )
                                    : ''
                            ),
                            (
                                (( 'on' === $show_author || 'on' === $show_date ) && 'on' === $show_categories)
                                    ? ' | '
                                    : ''
                            ),
                            (
                                'on' === $show_categories
                                    ? get_the_category_list(', ')
                                    : ''
                            ),
                            (
                                (( 'on' === $show_author || 'on' === $show_date || 'on' === $show_categories ) && 'on' === $show_comments)
                                    ? ' | '
                                    : ''
                            ),
                            (
                                'on' === $show_comments
                                    ? sprintf( esc_html( _nx( '%s Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder' ) ), number_format_i18n( get_comments_number() ) )
                                    : ''
                            )
                        );
                    }

                    /**
                     * Post content
                     */

                    ob_start();

                    echo '<div class="post-content">';
                    global $et_pb_rendering_column_content;

                    $post_content = et_strip_shortcodes( et_delete_post_first_video( get_the_content() ), true );

                    $et_pb_rendering_column_content = true;

                    if ( 'on' === $show_content ) {
                        global $more;

                        // page builder doesn't support more tag, so display the_content() in case of post made with page builder
                        if ( et_pb_is_pagebuilder_used( get_the_ID() ) ) {
                            $more = 1;
                            echo apply_filters( 'the_content', $post_content );
                        } else {
                            $more = null;
                            echo apply_filters( 'the_content', et_delete_post_first_video( get_the_content( esc_html__( 'read more...', 'et_builder' ) ) ) );
                        }
                    } else {
                        if ( has_excerpt() ) {
                            the_excerpt();
                        } else {
                            echo wpautop( et_delete_post_first_video( strip_shortcodes( truncate_post( 270, false, '', true ) ) ) );
                        }
                    }

                    $et_pb_rendering_column_content = false;

                    if ( 'on' !== $show_content ) {
                        $more = 'on' == $show_more ? sprintf( ' <a href="%1$s" class="more-link" >%2$s</a>' , esc_url( get_permalink() ), esc_html__( 'read more', 'et_builder' ) )  : '';
                        echo $more;
                    }

                    echo '</div>';

                    $output_content = ob_get_clean();
                
                }  // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery'
                
                $output[] = array(
                    'graphic' => $output_graphic,
                    'title' => $output_title,
                    'post_meta' => $output_post_meta,
                    'content' => $output_content,
                    'classes' => $no_thumb_class,
                );
                
			} // endwhile
            
            ob_start();
            
            if (!empty($output)) {

                if ( 'off' === $fullwidth ) {
                    echo '<div class="et_pb_salvattore_content" data-columns>';
                }

                ?>

                <?php foreach ($output as $post_content): ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post clearfix ' . $post_content['classes'] . ' ' . $overlay_class ); ?>>

                    <?php
                    vprintf($output_format, $post_content);
                    ?>

                </article> <!-- .et_pb_post -->

                <?php endforeach; ?>

                <?php

                if ( 'on' === $show_pagination && ! is_search() ) {
                    if ( function_exists( 'wp_pagenavi' ) ) {
                        wp_pagenavi();
                    } else {
                        if ( et_is_builder_plugin_active() ) {
                            include( ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php' );
                        } else {
                            get_template_part( 'includes/navigation', 'index' );
                        }
                    }

                    echo '</div> <!-- .et_pb_posts -->';

                    $container_is_closed = true;
                }

                if ( 'off' === $fullwidth ) {
                    echo '</div><!-- .et_pb_salvattore_content -->';
                }
            }

            $posts = ob_get_contents();
			
		} else {
            
            ob_start();
            
			if ( et_is_builder_plugin_active() ) {
				include( ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php' );
			} else {
				get_template_part( 'includes/no-results', 'index' );
			}
            
            $posts = ob_get_contents();
		}

		wp_reset_query();

		ob_end_clean();

		$class = " ws_et_pb_posts et_pb_module et_pb_bg_layout_{$background_layout}";

		$output = sprintf(
			'<div%5$s class="%1$s%3$s%6$s%7$s%9$s%11$s">
				%10$s
				%8$s
				<div class="et_pb_ajax_pagination_container">
					%2$s
				</div>
			%4$s',
			( 'on' === $fullwidth ? 'et_pb_posts' : 'et_pb_blog_grid clearfix' ),
			$posts,
			esc_attr( $class ),
			( ! $container_is_closed ? '</div> <!-- .et_pb_posts -->' : '' ),
			( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
			( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
			'' !== $video_background ? ' et_pb_section_video et_pb_preload' : '',
			$video_background,
			'' !== $parallax_image_background ? ' et_pb_section_parallax' : '',
			$parallax_image_background,
			'on' === $fullwidth ? $this->get_text_orientation_classname() : ''
		);

		if ( 'on' !== $fullwidth ) {
			$output = sprintf( '<div class="et_pb_blog_grid_wrapper">%1$s</div>', $output );
		}

		// Restore $wp_filter
		$wp_filter = $wp_filter_cache;
		unset($wp_filter_cache);

		return $output;
    }
}

new WS_Builder_Module_Blog();