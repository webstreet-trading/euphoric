(function ($) {
    $(document).ready(function () {

        /*
         * function to insert png image into url
         */
        function img_url(img) {            
            return 'url(../wp-content/themes/Euphoric/assets/img/' + img + ')';
        }

        /*
         * Inject ids on schedule a meeting header to be able to open modals
         */
//        $('#header_modal_btns .et_pb_button_one').attr('id', 'modal-desktop-schedule');
//        $('#header_modal_btns .et_pb_button_two').attr('id', 'modal-mobile-schedule');
        

        $('.investors_slider .et_pb_column').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: false,
            dots: true,
            infinite: true,
            slidesToScroll: 2,
            slidesToShow: 2,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToScroll: 1,
                        slidesToShow: 1
                    }
                }]
        });

        $("#modal-home-desktop-ebook").animatedModal({
            modalTarget: "modal-home-ebook-desktop-content",
            animatedIn: 'fadeIn',
            animatedOut: 'fadeOut'
        });

        $("#modal-home-mobile-ebook").animatedModal({
            modalTarget: "modal-home-ebook-mobile-content",
            animatedIn: 'fadeIn',
            animatedOut: 'fadeOut'
        });

        $("#modal-desktop-ebook").animatedModal({
            modalTarget: "modal-ebook-desktop-content",
            animatedIn: 'fadeIn',
            animatedOut: 'fadeOut'
        });

        $("#modal-mobile-ebook").animatedModal({
            modalTarget: "modal-ebook-mobile-content",
            animatedIn: 'fadeIn',
            animatedOut: 'fadeOut'
        });

        $("#modal-desktop-schedule").animatedModal({
            modalTarget: "modal-schedule-desktop-content",
            animatedIn: 'fadeIn',
            animatedOut: 'fadeOut'
        });

        $("#modal-mobile-schedule").animatedModal({
            modalTarget: "modal-schedule-mobile-content",
            animatedIn: 'fadeIn',
            animatedOut: 'fadeOut'
        });

        
        /*
         * Global vars
         */
        var animation_end = 'webkitAnimationEnd oanimationend msAnimationEnd animationend';
        var transition_end = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';

        /*
         * Show optin
         */
        $('.optin_cover_btn').click(function (e) {
            e.preventDefault();
            var keep_height = $('.row_optin_cover').outerHeight();
            $('.optin_section').height(keep_height);
            $('.row_optin_cover').addClass('animated flipOutX');
            $('.row_optin_cover').one(animation_end, function () {
                $('.row_optin_cover').css('display', 'none');
                $('.row_optin_proper').css('display', 'block');
                $('.row_optin_proper').addClass('animated flipInX');
            });

        });

        /*
         * Section overview criteria buttons toggle
         */
//        $('#section_overview_criteria .btn-toggle-hollow-yellow').click(function (e) {
//            e.preventDefault();
//            $('#section_overview_criteria .et_pb_button').removeClass('active');
//            $(this).addClass('active');
//        });

        /*
         * circle buttons equal height and width
         */
        $(window).on('load resize', function () {            
            if ($('.desktop .btn_circle').length) {
                var btn_width = $('.desktop .btn_circle')[0].getBoundingClientRect().width;
                $('.desktop .btn_circle').height(btn_width - 2);
            }
        });

        /*
         * circle buttons active toggle
         */
        $('.desktop.row_section12_btns .btn_circle').click(function () {
            var this_id = !$(this).hasClass('active') ? '#' + $(this).attr('id') : null;

            $('.desktop.row_section12_btns .btn_circle').removeClass('active');
            $(this).addClass('active');

            if (this_id !== null) {
                $('.row_circle_btn_content .circle_content.active').addClass('animated fadeOut');
                $('.row_circle_btn_content .circle_content.active').one(animation_end, function () {
                    $(this).removeClass('fadeOut animated active');
                    $(this_id + '-content').addClass('active animated fadeIn');
                    $(this_id + '-content').one(animation_end, function () {
                        $(this).removeClass('animated fadeIn');
                    });
                });
            }
        });

        $('.invest_row .btn_circle').click(function () {
            var this_id = !$(this).hasClass('active') ? '#' + $(this).attr('id') + '-content' : null;
            var this_img = !$(this).hasClass('active') ? $(this).attr('id') + '-img.jpg': null;

            $('.invest_row .btn_circle').removeClass('active');
            $(this).addClass('active');

            if (this_id !== null) {
                $('.btn_cirlcle_content.active').addClass('animated fadeOut');
                $('.btn_cirlcle_content.active').one(animation_end, function () {
                    $(this).removeClass('active animated fadeOut');
                    $(this_id).addClass('active animated fadeIn');
                    $(this_id).one(animation_end, function () {
                        $(this).removeClass('animated fadeIn');
                    });
                });
            }
            
            if ($(this).attr('id') === 'holiday-homes') {
                $('#modal-desktop-ebook').parent().addClass('animated fadeIn');
                $('#modal-desktop-ebook').parent().one(animation_end, function () {
                    $(this).css({
                        'opacity': 1
                    }).removeClass('animated fadeIn');
                });
            } else {
                if ($('#modal-desktop-ebook').parent().css('opacity') == 1) {
                    $('#modal-desktop-ebook').parent().addClass('animated fadeOut');
                    $('#modal-desktop-ebook').parent().one(animation_end, function () {
                        $(this).css({
                            'opacity': 0
                        }).removeClass('animated fadeOut');
                    });
                }
            }
            
            if (this_img !== null) {
                $('#backgr_fade').addClass('animated fadeIn');
                $('#backgr_fade').one(animation_end, function () {
                    $(this).css('opacity', 1).removeClass('animated fadeIn');
                    $('#col_toggle_img').css('background-image', img_url(this_img));
                    $(this).addClass('animated fadeOut');
                    $(this).one(animation_end, function () {
                        $(this).removeClass('animated fadeOut');
                        $(this).css('opacity', 0);
                    });
                });
                
            }
        });

        $('.tabs_hospitality_assets .et_pb_tabs_controls li').click(function () {
            if (!$(this).hasClass('et_pb_tab_0')) {
                $('#modal-mobile-ebook').parent().css('display', 'none');
            } else {
                $('#modal-mobile-ebook').parent().css('display', 'block');
            }
        });

//        if ($('.tabs_hospitality_assets .et_pb_tabs_controls .et_pb_tab_0').hasClass('et_pb_tab_active')) {
//            
//            $('#modal-mobile-ebook').parent().css('display', 'block');
//            
//        } else {
//            
//            $('#modal-mobile-ebook').parent().css('display', 'none');
//            
//        }

        /*
         * Append span element to accordion item title
         */
        $('.et_pb_toggle_title').each(function () {
            var this_html = '<span>' + $(this).html() + '</span>';
            $(this).html(this_html);
        });

        /*
         * testing slide button
         */
        $('.slidein_btn').click(function (e) {
            e.preventDefault();

            var content_id = '#' + $(this).attr('id') + '-content';
            var $current_content_col = $(this).parent().parent().next();

            if (!$(this).hasClass('active') && !$current_content_col.hasClass('active_content')) {

                $('.slidein_btn').removeClass('active');
                $(this).addClass('active');
                $('.col_slidein').addClass('active_content');

                $(content_id).css({
                    'display': 'table',
                    'z-index': 15
                }).addClass('active');
                $(content_id).find('.et_pb_text_inner').addClass('animated fadeIn');
                $(content_id).find('.et_pb_text_inner').one(animation_end, function () {
                    $(this).css('opacity', 1);
                    $(this).removeClass('animated fadeIn');
                });

            } else if (!$(this).hasClass('active') && $current_content_col.hasClass('active_content')) {

                $('.text_slidein_content.active').find('.et_pb_text_inner').addClass('animated fadeOut');
                $('.text_slidein_content.active').find('.et_pb_text_inner').one(animation_end, function () {
                    $(this).css('opacity', 0);
                    $(this).removeClass('animated fadeOut');
                    $(this).parent().css({
                        'display': 'none',
                        'z-index': 1
                    }).removeClass('active');
                });

                $('.slidein_btn').removeClass('active');
                $(this).addClass('active');

                $(content_id).css({
                    'display': 'table',
                    'z-index': 15
                }).addClass('active');
                $(content_id).find('.et_pb_text_inner').addClass('animated fadeIn');
                $(content_id).find('.et_pb_text_inner').one(animation_end, function () {
                    $(this).css('opacity', 1);
                    $(this).removeClass('animated rollOut');
                });

            } else if ($(this).hasClass('active') && $current_content_col.hasClass('active_content')) {

                $('.text_slidein_content.active').find('.et_pb_text_inner').addClass('animated fadeOut');
                $('.col_slidein').removeClass('active_content');
                $('.slidein_btn').removeClass('active');
                $('.text_slidein_content.active').find('.et_pb_text_inner').one(animation_end, function () {
                    $(this).css('opacity', 0);
                    $(this).removeClass('animated fadeOut');
                    $(this).parent().css({
                        'display': 'none',
                        'z-index': 1
                    }).removeClass('active');
                });

            }
        });
                    
        /*
         * Only one read more toggle open at a time
         */
        $('.toggle_person .et_pb_toggle_title').click( function(e){
            e.preventDefault();
            $('.toggle_person').addClass('et_pb_toggle_close');
            $('.toggle_person .et_pb_toggle_content').slideUp(800);
        });
        
        /*
         * Typer
         */        

//        $('[data-typer-targets]').typer({
//            typeSpeed: 1000
//        });

        $('#home_type_1').typer();
        setTimeout(function () {
            $('#home_type_2').css('opacity', 1);
            $('#home_type_2').typer();
        }, 5500);
        
        $('#section_type_1').css('opacity', 1);
        $('#section_type_1').typer();
        
        $('#invest_type_1').typer();
        setTimeout(function () {
            $('#invest_type_2').css('opacity', 1);
            $('#invest_type_2').typer();
        }, 5500);        

        $('#estate_type_1').typer();
        setTimeout(function () {
            $('#estate_type_2').css('opacity', 1);
            $('#estate_type_2').typer();
        }, 5500);
        
        $('#registry_type_1').css('opacity', 1);
        $('#registry_type_1').typer();
        
        $('#schedule_type_1').typer();
        setTimeout(function () {
            $('#schedule_type_2').css('opacity', 1);
            $('#schedule_type_2').typer();
            setTimeout(function () {
                $('#header_modal_btns .et_pb_more_button').css('opacity', 1);
            }, 7500);
        }, 6000);

        /*
         * svg diagram Load animation
         */
        
        function get_line_dasharray (el) {            
            return $(el).css('stroke-dasharray').slice(0, -2);
        }
        
        function draw_line_css () {            
            return {'stroke-dashoffset': 0};
        }
        
        function ws_fadein_start (el) {
            return $(el).addClass('animated fadeIn');
        }
        
        function ws_fadein_finish (el) {
            return $(el).css('opacity', 1).removeClass('animated fadeIn');
        }
        
        $(window).on('load scroll', function () {                       
            
            if ($('#main_diagram').length) {
                var window_pos = $(this).scrollTop();
                var svg_pos = $('#main_diagram').offset().top;
                var diagram_height = $('#main_diagram').height();
                var animation_speed = 500;

                if (!$('.row_diagram').hasClass('diagram_loaded')) {
                    if (window_pos + (diagram_height * 1) >= svg_pos) {


                        ws_fadein_start('#you');
                        $('#you').one(animation_end, function () {
                            ws_fadein_finish(this);
                            $('.you_arrow_contain_1 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                $('.you_arrow_contain_1 .arrow_shadow').addClass('display_arrow_shadow');
                                $('.you_arrow_contain_1 .arrow_shadow').one(transition_end, function () {
                                    ws_fadein_start('#you_euphoric_line_description');
                                    $('#you_euphoric_line_description').one(animation_end, function () {
                                        ws_fadein_finish(this);
                                        ws_fadein_start('#euphoric');
                                        $('#euphoric').one(animation_end, function () {
                                            ws_fadein_finish(this);

                                            //animation towards #mannco from #euphoric
                                            $('.euphoric_arrow_contain_1 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                $('.euphoric_arrow_contain_1 .arrow_shadow').addClass('display_arrow_shadow');
                                                $('.euphoric_arrow_contain_1 .arrow_shadow').one(transition_end, function () {
                                                    ws_fadein_start('#euphoric_mannco_line_description');
                                                    $('#euphoric_mannco_line_description').one(animation_end, function () {
                                                        ws_fadein_finish(this);
                                                        ws_fadein_start('#mannco');
                                                        $('#mannco').one(animation_end, function () {
                                                            ws_fadein_finish(this);
                                                        });
                                                    });
                                                });
                                            });

                                            //animation towards #mannco from #euphoric
                                            $('.euphoric_arrow_contain_3 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                $('.euphoric_arrow_contain_3 .arrow_shadow').addClass('display_arrow_shadow');
                                                $('.euphoric_arrow_contain_3 .arrow_shadow').one(transition_end, function () {
                                                    ws_fadein_start('#euphoric_spv_line_description');
                                                    $('#euphoric_spv_line_description').one(animation_end, function () {
                                                        ws_fadein_finish(this);
                                                        ws_fadein_start('#spv');
                                                        $('#spv').one(animation_end, function () {
                                                            ws_fadein_finish(this);

                                                            //animation towards #euphoric from #spv
                                                            $('.spv_arrow_contain_2 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                                $('.spv_arrow_contain_2 .arrow_shadow').addClass('display_arrow_shadow');
                                                                $('.spv_arrow_contain_2 .arrow_shadow').one(transition_end, function () {
                                                                    ws_fadein_start('#spv_euphoric_line_description');
                                                                    $('#spv_euphoric_line_description').one(animation_end, function () {
                                                                        ws_fadein_finish(this);
                                                                    });
                                                                });
                                                            });

                                                            //animation towards #leisure from #spv
                                                            $('.spv_arrow_contain_1 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                                $('.spv_arrow_contain_1 .arrow_shadow').addClass('display_arrow_shadow');
                                                                $('.spv_arrow_contain_1 .arrow_shadow').one(transition_end, function () {
                                                                    ws_fadein_start('#spv_leisure_line_description');
                                                                    $('#spv_leisure_line_description').one(animation_end, function () {
                                                                        ws_fadein_finish(this);
                                                                        ws_fadein_start('#leisure');
                                                                        $('#leisure').one(animation_end, function () {
                                                                            ws_fadein_finish(this);

                                                                            //animation towards #property from #leisure
                                                                            $('.leisure_arrow_contain_1 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                                                $('.leisure_arrow_contain_1 .arrow_shadow').addClass('display_arrow_shadow');
                                                                                $('.leisure_arrow_contain_1 .arrow_shadow').one(transition_end, function () {
                                                                                    ws_fadein_start('#leisure_propery_line_description');
                                                                                    $('#leisure_propery_line_description').one(animation_end, function () {
                                                                                        ws_fadein_finish(this);
                                                                                    });
                                                                                });
                                                                            });

                                                                        });
                                                                    });
                                                                });
                                                            });

                                                            //animation towards #property from #spv
                                                            $('.spv_arrow_contain_3 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                                $('.spv_arrow_contain_3 .arrow_shadow').addClass('display_arrow_shadow');
                                                                $('.spv_arrow_contain_3 .arrow_shadow').one(transition_end, function () {
                                                                    ws_fadein_start('#spv_property_line_description');
                                                                    $('#spv_property_line_description').one(animation_end, function () {
                                                                        ws_fadein_finish(this);
                                                                        ws_fadein_start('#property');
                                                                        $('#property').one(animation_end, function () {
                                                                            ws_fadein_finish(this);

                                                                            //animation towards #spv from #property
                                                                            $('.property_arrow_contain_2 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                                                $('.property_arrow_contain_2 .arrow_shadow').addClass('display_arrow_shadow');
                                                                                $('.property_arrow_contain_2 .arrow_shadow').one(transition_end, function () {
                                                                                    ws_fadein_start('#property_spv_line_description');
                                                                                    $('#property_spv_line_description').one(animation_end, function () {
                                                                                        ws_fadein_finish(this);
                                                                                    });
                                                                                });
                                                                            });

                                                                            //animation towards #registry from #property
                                                                            $('.property_arrow_contain_bottom .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                                                $('.property_arrow_contain_bottom .arrow_shadow').addClass('display_arrow_shadow');
                                                                                $('.property_arrow_contain_bottom .arrow_shadow').one(transition_end, function () {

                                                                                    ws_fadein_start('#property_registry_line_description_1');
                                                                                    $('#property_registry_line_description_1').one(animation_end, function () {
                                                                                        ws_fadein_finish(this);
                                                                                    });

                                                                                    ws_fadein_start('#registry');
                                                                                    $('#registry').one(animation_end, function () {
                                                                                        ws_fadein_finish(this);

                                                                                        //animation towards #you from #registry
                                                                                        $('.property_arrow_contain_1 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                                                            $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                                                            $('.property_arrow_contain_1 .arrow_shadow').addClass('display_arrow_shadow');
                                                                                        });

                                                                                    });

                                                                                });
                                                                            });

                                                                        });
                                                                    });
                                                                });
                                                            });

                                                        });
                                                    });
                                                });
                                            });

                                            //animation towards #you from #euphoric
                                            $('.euphoric_arrow_contain_2 .arrow_line').animate(draw_line_css(), animation_speed, function () {
                                                $(this).parent().find('.arrow_point').addClass('display_svg_point');
                                                $('.euphoric_arrow_contain_2 .arrow_shadow').addClass('display_arrow_shadow');
                                                $('.euphoric_arrow_contain_2 .arrow_shadow').one(transition_end, function () {
                                                    ws_fadein_start('#euphoric_you_line_description');
                                                    $('#euphoric_you_line_description').one(animation_end, function () {
                                                        ws_fadein_finish(this);
                                                    });
                                                });
                                            });

                                        });
                                    });
                                });
                            });
                        });

                        $('.row_diagram').addClass('diagram_loaded');

                    }

                }
            }

        });

        

        /*
         * svg diagram Hover animation
         */
        
//        if ($('.row_diagram').hasClass('diagram_loaded')) {
//
//            $('.block_contain').hover(
//                function () {
//
//                    console.log('hover on');
//
//                    var this_id = $(this).attr('id');
//
//                    var points_to = $(this).data('pointsto');
//                    var points = points_to.split(" ");
//
//                    $('.arrow_contain').css('opacity', .5);
//                    $('.block_contain').css('opacity', .5);
//                    $('.line_description').css('opacity', .5);
//                    $('.arrow_contain').find('.arrow_shadow').css('opacity', 0);
//                    $('.block_contain').find('.block_shadow').css('opacity', 0);
//
//                    $(this).css('opacity', 1);
//                    $(this).find('.block_shadow').css('opacity', 0.6000000000000001);
//
//                    $('.' + this_id + '_arrow_contain ').css('opacity', 1);
//                    $('.' + this_id + '_arrow_contain ').find('.arrow_shadow').css('opacity', 0.6000000000000001);
//                    $('.' + this_id + '_description').css('opacity', 1);
//
//                    for (var i = 0; i < points.length; i++) {
//                        $('#' + points[i]).css('opacity', 1);
//                        $('#' + points[i]).find('.block_shadow').css('opacity', 0.6000000000000001);
//                    }
//
//                    if (this_id === 'registry') {
//                        $('.property_arrow_contain').css('opacity', 1);
//                        $('.property_arrow_contain').find('.arrow_shadow').css('opacity', 0.6000000000000001);
//                        $('.property_arrow_contain_bottom').css('opacity', .5);
//                        $('.property_arrow_contain_bottom').find('.arrow_shadow').css('opacity', 0);
//                    }
//
//                },
//                function () {
//
//                    console.log('hover off');
//
//                    $('.arrow_contain').css('opacity', 1);
//                    $('.block_contain').css('opacity', 1);
//                    $('.arrow_contain').find('.arrow_shadow').css('opacity', 0.6000000000000001);
//                    $('.block_contain').find('.block_shadow').css('opacity', 0.6000000000000001);
//                    $('.property_arrow_contain_bottom').css('opacity', 1);
//                    $('.property_arrow_contain_bottom').find('.arrow_shadow').css('opacity', 0.6000000000000001);
//                    $('.line_description').css('opacity', 1);
//
//                }
//            );
//
//        }

        /**
         * Infusionsoft form validation. Based on Divi Contact Form
         */
        
        var $infusionsoft_forms = $('form.infusionsoft-form');

        if ($infusionsoft_forms.length) {
            $infusionsoft_forms.each(function () {
                var $infusionsoft_form = $(this),
                    et_email_reg = /^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/;

                $infusionsoft_form.find('input[type=checkbox]').on('change', function () {
                    var $checkbox = $(this);
                    var $checkbox_field = $checkbox.siblings('input[type=text]:first');
                    var is_checked = $checkbox.prop('checked');

                    $checkbox_field.val(is_checked ? $checkbox_field.data('checked') : $checkbox_field.data('unchecked'));
                });

                $infusionsoft_form.on('submit', function (event) {
                    var $this_contact_form = $(this),
                            $this_inputs = $this_contact_form.find('input[type=text], .et_pb_checkbox_handle, .et_pb_contact_field[data-type="radio"], textarea, select'),
                            this_et_contact_error = false,
                            inputs_list = [];

                    $this_inputs.removeClass('et_contact_error');

                    var hidden_fields = [];

                    $this_inputs.each(function () {
                        var $this_el = $(this);
                        var $this_wrapper = false;

                        if ('checkbox' === $this_el.data('field_type')) {
                            $this_wrapper = $this_el.parents('.et_pb_contact_field');
                            $this_wrapper.removeClass('et_contact_error');
                        }

                        if ('radio' === $this_el.data('type')) {
                            $this_el = $this_el.find('input[type="radio"]');
                            $this_wrapper = $this_el.parents('.et_pb_contact_field');
                        }

                        var this_id = $this_el.attr('id');
                        var this_val = $this_el.val();
                        var this_label = $this_el.siblings('label:first').text();
                        var field_type = typeof $this_el.data('field_type') !== 'undefined' ? $this_el.data('field_type') : 'text';
                        var required_mark = typeof $this_el.data('required_mark') !== 'undefined' ? $this_el.data('required_mark') : 'not_required';
                        var original_id = typeof $this_el.data('original_id') !== 'undefined' ? $this_el.data('original_id') : '';
                        var unchecked = false;

                        // Escape double quotes in label
                        this_label = this_label.replace(/"/g, "&quot;");

                        // radio field properties adjustment
                        if ('radio' === field_type) {
                            if (0 !== $this_wrapper.find('input[type="radio"]').length) {
                                field_type = 'radio';

                                var $firstRadio = $this_wrapper.find('input[type="radio"]:first');

                                required_mark = typeof $firstRadio.data('required_mark') !== 'undefined' ? $firstRadio.data('required_mark') : 'not_required';

                                this_val = '';
                                if ($this_wrapper.find('input[type="radio"]:checked')) {
                                    this_val = $this_wrapper.find('input[type="radio"]:checked').val();
                                }
                            }

                            this_label = $this_wrapper.find('.et_pb_contact_form_label').text();
                            this_id = $this_wrapper.find('input[type="radio"]:first').attr('name');
                            original_id = $this_wrapper.attr('data-id');

                            if (0 === $this_wrapper.find('input[type="radio"]:checked').length) {
                                unchecked = true;
                            }
                        }

                        // radio field properties adjustment
                        if ('checkbox' === field_type) {
                            this_val = '';

                            if (0 !== $this_wrapper.find('input[type="checkbox"]').length) {
                                field_type = 'checkbox';

                                var $checkboxHandle = $this_wrapper.find('.et_pb_checkbox_handle');

                                required_mark = typeof $checkboxHandle.data('required_mark') !== 'undefined' ? $checkboxHandle.data('required_mark') : 'not_required';

                                if ($this_wrapper.find('input[type="checked"]:checked')) {
                                    this_val = [];
                                    $this_wrapper.find('input[type="checkbox"]:checked').each(function () {
                                        this_val.push($(this).val());
                                    });

                                    this_val = this_val.join(', ');
                                }
                            }

                            $this_wrapper.find('.et_pb_checkbox_handle').val(this_val);

                            this_label = $this_wrapper.find('.et_pb_contact_form_label').text();
                            this_id = $this_wrapper.find('.et_pb_checkbox_handle').attr('name');
                            original_id = $this_wrapper.attr('data-id');

                            if (0 === $this_wrapper.find('input[type="checkbox"]:checked').length) {
                                unchecked = true;
                            }
                        }

                        // Store the labels of the conditionally hidden fields so that they can be
                        // removed later if a custom message pattern is enabled
                        if (!$this_el.is(':visible') && 'hidden' !== $this_el.attr('type') && 'radio' !== $this_el.attr('type')) {
                            hidden_fields.push(original_id);
                            return;
                        }

                        if (('hidden' === $this_el.attr('type') || 'radio' === $this_el.attr('type')) && !$this_el.parents('.et_pb_contact_field').is(':visible')) {
                            hidden_fields.push(original_id);
                            return;
                        }

                        // add current field data into array of inputs
                        if (typeof this_id !== 'undefined') {
                            inputs_list.push({'field_id': this_id, 'original_id': original_id, 'required_mark': required_mark, 'field_type': field_type, 'field_label': this_label});
                        }

                        // add error message for the field if it is required and empty
                        if ('required' === required_mark && ('' === this_val || true === unchecked)) {

                            if (false === $this_wrapper) {
                                $this_el.addClass('et_contact_error');
                            } else {
                                $this_wrapper.addClass('et_contact_error');
                            }

                            this_et_contact_error = true;
                        }

                        // add error message if email field is not empty and fails the email validation
                        if ('email' === field_type) {
                            // remove trailing/leading spaces and convert email to lowercase
                            var processed_email = this_val.trim().toLowerCase();
                            var is_valid_email = et_email_reg.test(processed_email);

                            if ('' !== processed_email && this_label !== processed_email && !is_valid_email) {
                                $this_el.addClass('et_contact_error');
                                this_et_contact_error = true;
                            }
                        }
                    });

                    if (this_et_contact_error) {
                        event.preventDefault();
                    }
                });
            });
        }
        
//        $('.blog_overlay_module_grid .et_pb_post').hover(function( event ) {
//            
//            var direction = $(this).entry({ e : event });
//            $('.blog_overlay_module_grid .et_pb_post').find('.entry-title').removeClass('out_left out_down out_right out_up');
//            $(this).find('.entry-title').addClass('in_' + direction);
//            
//        }, function( event ) {
//                
//            var direction = $(this).entry({ e : event });
//            $('.blog_overlay_module_grid .et_pb_post').find('.entry-title').removeClass('in_left in_down in_right in_up');
//            $(this).find('.entry-title').addClass('out_' + direction);
//            
//        });
//        $('.blog_overlay_module_grid .et_pb_post').mouseenter(function (event) {
//            var direction = $(this).entry({ e : event });
//            $(this).addClass(direction);
//        });

        /*
         * Size blog grid cols to be equal
         */
//        $(window).one('load resize', function () {
//            
//            var max_height = 0;
//            
//            $('.blog_overlay_module_grid .et_pb_salvattore_content .size-1of2').each(function () {
//                var this_height = $(this).height();
//                max_height = this_height > max_height ? this_height : max_height;
//            });
//            
//            $('.blog_overlay_module_grid .et_pb_salvattore_content .size-1of2').each(function () {
//                $(this).height(max_height);
//            });
//            
//            $('.blog_overlay_module_grid .et_pb_salvattore_content .size-1of3').each(function () {
//                var this_height = $(this).height();
//                max_height = this_height > max_height ? this_height : max_height;
//            });
//            
//            $('.blog_overlay_module_grid .et_pb_salvattore_content .size-1of3').each(function () {
//                $(this).height(max_height);
//            });
//            
//        });
        
        $('.blog_header .et_pb_slide .et_pb_slide_overlay_container').each(function () {
            var this_html = '<a href="#blog-content"></a>';
            $(this).html(this_html);
        });
        
        var menu_height = ($(window).width() + 17) > 980 ? 99 : 0;
        
        $('.blog_header .et_pb_slide .et_pb_slide_overlay_container a').each(function () {
            $(this).click(function (e) {
                e.preventDefault();
                $('html, body').animate({
                    'scrollTop': ($('#blog-content').offset().top - menu_height)
                }, 500);
            });            
        });
        
    });
})(jQuery);