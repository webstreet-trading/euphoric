<?php
/**
 * Sensei Content wrapper end
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>

            </div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->