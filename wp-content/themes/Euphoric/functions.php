<?php

// run includes
include('ws/divi/functions.php');

/*
 * Disable Divi builder on posts
 */

function ws_remove_divi_page_builder_post() {
    $screen = get_current_screen();
    if ($screen->post_type == 'post') {
        echo '<style>.et_pb_toggle_builder_wrapper{display:none;}</style>';
    }
}
add_action('admin_head', 'ws_remove_divi_page_builder_post');

/*
 * Add Divi module by id
 * 
 * @param $id. Divi library id
 * @return viod
 */
function ws_divi_module($id){
    
    echo do_shortcode('[divimodule id="' . $id . '"]');
    
}

/*
 * Limit the excerpt to a number of words
 * 
 * @param int $limit. Number of words
 * 
 * @return string $excerpt
 */
function ws_limit_excerpt($limit) {
    
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    
    if (count($excerpt) >= $limit) {
        
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
        
    } else {
        
        $excerpt = implode(" ", $excerpt);
        
    }
    
    $excerpt = preg_replace('`[[^]]*]`', '', $excerpt);
    
    return $excerpt;
}

/*
 * New post meta
 */
function ws_post_meta() {
    
    $author = '<span class="ws_author">' . get_the_author() . '</span>';
    $date = '<span class="ws_date">' . get_the_date('j M, Y') . '</span>';
    $read_time = do_shortcode('[rt_reading_time postfix="min read" postfix_singular="min"]');
    $divider = '<span class="ws_meta_div"></span>';
    
    echo '<p class="ws_single_meta">' . $author . $divider . $date . $divider . $read_time . '</p>';

}